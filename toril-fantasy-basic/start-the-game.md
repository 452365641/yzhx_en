---
description: G
---

# Start the Game

Players need to buy at least 5 Hero NFT, then transfer Hero NFT from "Wallet Assets" to ""Game Assets" before they can enter the game for PVP or PVE adventures.

The specific operation steps are as follows:

**Step 1:** Enter the official website (URL: http://beta.toril.io/ ). Click "Login" to connect your wallet.

![](../.gitbook/assets/3.1.png)

![](../.gitbook/assets/3.2.png)

**Step 2:** Click on "Market" and select 5 preferred heroes NFT in the Hero Mall to purchase.

![](../.gitbook/assets/3.3.png)

**Step 3:** After successful purchase, click "Assets", select the "Add to Game" button of the purchased hero in the "Wallet Assets" column, and click "Confirm" button in the pop-up page.

![](../.gitbook/assets/3.4.png)

![](../.gitbook/assets/3.5.png)

**Step 4:** Click "Game Assets", you can see the hero NFT has been successfully transferred (you need to transfer all 5 hero NFT into the game assets).

![](../.gitbook/assets/3.6.png)

**Step 5:** Click the "Download" button in the navigation bar to download the APK package of the game, and then you can enter the game after the installation is completed.

![](../.gitbook/assets/3.7.png)

