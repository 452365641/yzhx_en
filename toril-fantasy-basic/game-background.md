# Game Background

In the world of Toril Fantasy, the civilization on the continent of Kufiseioni resembles that of late medieval Europe. The heroes living on this continent are mainly divided into six major races and five major professions. Due to the law of mutual restraint between the races, the six races have always maintained a relatively peaceful coexistence, and even in order to complete various tasks, the races will also cooperate.

But when the powerful evil power of the Red Meteor re-emerged and was controlled by a group of cultists trying to destroy the world, such peace was soon broken, and the races began to strife. In order to fight the cultists and save the world, mankind must once again unite the six races in a call to arms.

By recruiting more race heroes and challenging various tasks, the team can be strong, while using the characteristics of the hero profession to determine the choice of team heroes, such as warriors can withstand most of the damage for the team, mages can provide the team with a large amount of mana output, etc.. Of course, humans can also use these heroes to achieve their own ambitions, such as through competitive confrontation with other teams, to obtain higher honor and material rewards, to achieve the goal of the strongest team.
