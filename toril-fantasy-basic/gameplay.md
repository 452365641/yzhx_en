# Gameplay

In "Toril Fantasy", players will play a brave and resourceful warrior, through strengthening the hero through the main plot finally complete the task of saving the world. Players can experience a variety of easy and fun PVE gameplay, such as: the Tower of Heaven, the mysterious catacombs, the chief challenge and so on, but also participate in personal PvP arena and 100 people PvP territory battle to feel the thrill of competition.



At the beginning of the game, the player's main task is to complete the main line level challenges, unlock the major copies, recruit more heroes.



In the middle of the game, as the difficulty of the level upgrades, players need to understand more about each hero's skill characteristics and career characteristics, while using the racial aura buffs to 5v5 strategy array, and then to challenge more and more difficult copies for the heroes to win more upgrade resources, only in this way, can continue to get stronger.



In the late game, when players have enough strength and strategic skills, then the alliance war will become the core gameplay, how to successfully confront and defeat other alliances will be a permanent challenge for each player.
