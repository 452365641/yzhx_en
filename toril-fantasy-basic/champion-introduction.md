# Champion Introduction

In Toril Fantasy, heroes are divided into six races and five professions. The six races are: United Kingdom (human race), Khanate tribe (barbarian race), Forest Alliance (elven race), Undead Heavenly Calamity (undead race), Heavenly Gods (divine race), and Abyssal Demons (demonic race). The five major professions are: Assassin, Mage, Ranger, Priest, and Warrior.

There are laws of restraint between races, as follows: human restrains barbarians, barbarians restrain elves, elves restrain undead, undead restrain humans, and gods and devils restrain each other.

The human race and the elves have 12 heroes, the barbarians and the undead have 11 heroes each, and the gods and devils have 3 heroes each. Next, we will talk about the background of each of the six races and choose one of the heroes as an example to introduce.

## **United Kingdom**

The United Kingdom is the most populous intelligent race in the world, and they are mainly clustered in the kingdom of Atlasia, with an oligarchic political system, ruled by a local king or lord. The United Kingdom consists mainly of humans, dwarves, halflings, and gnomes, with humans involved in nearly all types of livelihoods: farming, trade, priests, etc. Dwarves are more involved in ore and forge work, while gnomes mostly perform gear-related work based on their racial talents. Under most areas, they are able to live together without hindrance, but in areas with more nobility, a trend similar to human supremacist thinking can occur.

**Champion: Suhas (Profession: Mage)**

****![](../.gitbook/assets/苏哈斯立绘.png)****

Although Suhas is old, bearded and seemingly uncontested, this scheming old man has been in the midst of years of brutal political struggle by noblemen and ministers and has become an evergreen tree in the political arena of the United Kingdom. At the same time, as the royal chief mage of Atlasia, Suhas's research on magic can be described as extraordinary, especially for the study of energy shaping and prophecy spells in the entire world is no other.

## Khanate Tribe

The Khanate tribe live mainly in the southern plains of the Gru Khanate and consist of Orcs, Beastmen, and some wanted criminals (such as bandits and pirates) who are not accepted by civilization, they are wild, mostly violent, and believe that the strongest is king, often fighting over livestock, prey, and pastures. They make their living in small nomadic groups, with men generally responsible for conquest and hunting, and women responsible for domestic and internal affairs, although there are a few women who also fight in all directions because of their great talent for fighting. Within the community, the basic implementation of clan politics, by the clan leader, elders and shamans meet to discuss politics, but also because there is no unified political institutions, so their areas are often many plunderers encamped.

**Champion: Glesia (Profession: Warrior)**

****![](../.gitbook/assets/格莱西雅.png)****

She is the chief of the "Winter Clan" in the northern winter ice region, born in the extreme cold, although there are countless ferocious beasts and magical creatures, but the fighting strength of Glesia can always lead her tribe to successfully defeat the enemy. At the same time, in order to survive, Glesia will also go to sack other barbarians and elves, this behavior in the peace era is easy to become the enemy of the entire continent. In order to survive in the future, Glesia signed an agreement with the ministers of the Gru Khanate, in which the Gru Khanate would forgive the past cruelty of the Rindon tribe, while the Rindon tribe would be required to provide all their own war power for that coming doom.

## **Forest Alliance**

The forest alliance led by the elves mainly lives in the forests of the northern part of the Gruhan Kingdom, and its members also include the Pixies, the tree people and some Orc tribes with a more peaceful temperament. They love the forest and are uncontested, with very little communication with the outside world and very little internal conflict, living a relatively quiet and peaceful life.

The elves are mainly engaged in traditional trades such as planting and bow making, while other races such as the Pixies and Tree Demons are not productive, but will join the army to fight for the forest when foreign enemies invade. The government of the Forest Alliance is a council of elders, ruled by a council of elf queens and elders, but there are also ambassadors from the other major races who participate in the council's deliberations.

**Champion: Alisha (Profession: Priest)**

****![](../.gitbook/assets/亚莉莎.png)****

From the southern mermaid tribe of the continent, Alisha had a happy family, but all this was destroyed by a businessman called Weeks, he led pirates and slavers to solve their own financial problems occupied their village, the men were slaughtered, women and children were bought by slavers, and Alisha was suffering from star disease can not be sold and was locked up under the dungeon, thinking that if Alisha can However, he forgot another quality of the star disease - if you survive the disease and do not die, you will certainly gain some kind of special ability. The end result was that he was torn apart alive by the current controlled by Arisa. In order to find her mother and her sold kin, Alisha went into battle and became one of the most famous mercenaries on the continent at the age of just a teenager.

## **Undead Scourge**

Most undead on the continent of Kufiseioni will be found in places where corpses and death gather, such as tombs and battlefield ruins. Of course there are also some powerful undead created with special circumstances, such as the vampires created by the first embrace and the sutured monsters created in the laboratory.

Most of the low-level undead are evil and crazy, and the only reason for their actions is their absolute hatred of the living. However, for high level undead with intelligence or even high intelligence, they may show signs of kindness towards others, and most of them were also strong in their race before they were born and retain a certain degree of outstanding personality after becoming undead.

**Champion: Noelle (Profession: Ranger)**

![](../.gitbook/assets/诺艾尔.png)

A powerful and lonely vampire, eager to talk to people to pass the time, but few people dare to approach her. Until she joined the mercenary group, Noelle's powerful strength gained the recognition and welcome of other members of the mercenary group.

## Celestials

The Divine Descendants are extra-terrestrial races from the Astral Plane who reside in the seven layers of the Heavenly Mountains, the place of supreme goodness in the universe. All the heavenly races are the embodiment of the belief in supreme goodness and pure positive energy, and therefore do not need to eat and toil. Each level of Heavenly Mountain is ruled by a powerful and benevolent celestial creature, and these rulers also form the Heavenly Council that leads Heavenly Mountain.

**Champion: Remiel (Profession: Priest)**

![](../.gitbook/assets/天使立绘.png)

He has served as an archangel for several years, shining with golden energy, specializing in healing and using divine light to blast the evil-doers. His merits were so outstanding that he earned the title of "God of Mercy" on the Seventh Heavenly Mountain. Whenever Remiel is called to the lower world in response to the main material world, it means that the impending disaster is enough to shake the foundation of the whole world, and this time the Red Meteor event has called Remiel to the lower world again.

## **Abyssal Dwarf**

Although, like the gods, the demons are also an extraterrestrial race from the astral world, they are incarnated by pure evil thoughts and live in the bottomless abyss, the most evil place in the universe. The demon lords here are the best in the endless evil struggle, as the demon lords will also carry out a constant struggle and betrayal, a long-lasting subordination and leadership relationship does not exist, not to mention the unity of the polity.

**Champion: Diablo (Profession: Assassin)**

![](../.gitbook/assets/恶魔立绘.png)

As the embodiment of fear, Diablo's point of existence is to bring fear to the main material world, and his crisp appearance is merely to give him a better understanding of the personality and vision of his victims. When Diablo joins your banner, then you have to be careful, whether he wants to understand you before bringing you endless fear or thinks your future will bring great fear to more people, who knows?
