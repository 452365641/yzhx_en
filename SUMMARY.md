# Table of contents

## Welcome to Toril Fantasy

* [Project Description](README.md)

## About Toril Fantasy

* [Blockchain Basics](about-toril-fantasy/blockchain-basics.md)
* [Wallet Guide](about-toril-fantasy/wallet-guide.md)
* [What is Toril Fantasy NFT](about-toril-fantasy/what-is-toril-fantasy-nft.md)
* [How to Get Toril Fantasy NFT](about-toril-fantasy/how-to-get-toril-fantasy-nft.md)
* [What is Mining Power](about-toril-fantasy/what-is-mining-power.md)

## Toril Fantasy Basic

* [Start the Game](toril-fantasy-basic/start-the-game.md)
* [Game Background](toril-fantasy-basic/game-background.md)
* [Champion Introduction](toril-fantasy-basic/champion-introduction.md)
* [Gameplay](toril-fantasy-basic/gameplay.md)

## Toril Fantasy - Play2Earn

* [Common Way about P2E](toril-fantasy-play2earn/common-way-about-p2e.md)

## Game Token

* [Page 1](game-token/page-1.md)

***

* [繁體中文](https://docs.toril.io/zh-tw/)
