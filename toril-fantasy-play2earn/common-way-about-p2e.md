# Common Way about P2E

Common P2E methods:

1、Participate in liquidity mining

2、Participate in NFT mining

3、Participate in mysterious treasure box subscription and trading

4、Participate in the game



**Participate in liquidity mining**

Participate in mining after pledging Torellcoin TC, the overall average annualization of the platform is currently at 300%, and there is a maximum of 4 times annualization acceleration available



**Participate in NFT mining**

Earnings depend on the total count of the heroes or NFTs you own Pledge heroes or equip NFT for mining, you can get a relatively stable return of 100% annualized.



**Participate in the purchase and trading of mysterious treasure chests**

The bid price of the chest is one-third of the average selling price, players can choose to sell the chest to earn the difference Players can also choose to open the mysterious treasure chests, which have the chance to obtain epic and mythical heroes or equipment of purple and orange grades that far exceed the selling price of the chests.



**Participate in the game**

This is a great Hangman card game that is highly entertaining and commercial. There are many ways for players to earn income in the game, such as daily push quests, daily challenge quests, arena leaderboards, siege battles, and many other game systems available to players to achieve play to earn P2E.
