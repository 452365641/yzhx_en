# Blockchain Basics

**What is** **DEFI?**

DEFI (decentralized finance), which is a financial application built on top of blockchain, aims to make financial activities available to everyone in the world, anytime and anywhere.

In the traditional financial industry, many financial institutions, such as banks, earn revenue by lending money and other financial actions, while returning interest to users.

DEFI, on the other hand, allows all the things previously done by financial institutions to be executed by code through the creation of smart contracts. This not only reduces the operational costs of banking institutions, users can be returned to more interest, but the contract is transparent, accessible and inclusive peer-to-peer financial system, minimizing the risk of trust and making it easier and more convenient for participants to access financing.

****

**What is NFT?**

NFT (Non-Fungible Token), which is the only form of cryptocurrency asset used to represent digital assets (including jpg and video clip forms), can be bought, sold and transferred.NFT assets can be games, artworks, domain names, collectibles, etc. It has the characteristics of indivisible, irreplaceable and unique.

If we understand NFT as a game prop, we can see that in traditional games, the game props purchased by users may be lost due to blocking or suspension of service, or the price may be reduced as the game party lowers the acquisition threshold. But in blockchain games, each NFT acquired by a player is permanent and unique, and truly owned, and its price is mainly influenced by the market, not the game publisher or operator.



**What is GAMEFI?**

GameFi, i.e. GAME+DeFi, also known as "gamified finance", is a business model that combines games and finance, presenting financial products in the form of games, gamifying the rules of DeFi, NFTing game props and derivatives, and adding traditional game play such as matchmaking and socialization to increase the entertainment and interactivity of participants. GAMEFI model can make users feel interesting and profitable at the same time.



The features of GameFi are:

1. the game is highly decentralized, anyone can actively participate in the development and construction of the game, even the original developer of the game does not have the right to control the development of the game and close the game.
2. High degree of freedom for players, they can vote through the game governance tokens, and players can vote through the game governance tokens and participate in the formulation and improvement of the game rules.
3. Earn-while-playing mode, the equipment or tools players win in the game can be sold as assets on the blockchain's NFT platform and trading platform.
4. Players can use their NFT to put into the game for mining and get blockchain Token.
