# Wallet Guide

Toril Fantasy is a BSC chain based project, so players need to create a BSC address to install a MetaMask wallet.

MetaMask is a plug-in ethereum wallet for use on Google Chrome. You just need to add the corresponding extension in Google Chrome, which is more convenient.

**Step 1:** Download and install MetaMask (URL: https://metamask.io/download.html). A fox logo will appear in the top right corner of your browser after the installation is complete. With just one click you can import your wallet or create a new one.

![](../.gitbook/assets/1.png)

**Step 2:** Add BSC chains. Since MetaMask does not list BSC chains by default, you need to add them manually. Click Blockchain Network at the top of the MetaMask interface and select Custom RPC.

![](../.gitbook/assets/2.png)

Fill in the following information and click 'Save'.

**Network:** Binance Smartchain

**URL:** https://bsc-dataseed.binance.org/

**Number:** 56

**Symbol：**BNB

**Website:** https://www.bscscan.com/

Later on, you can see that Binance Smartchain has an additional option. You can import your existing BSC account private key into MetaMask.
