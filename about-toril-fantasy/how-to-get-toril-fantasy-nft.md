# How to Get Toril Fantasy NFT

The Toril NFT Marketplace is now open. Players can buy Toril NFT through the Toril NFT Marketplace. here are the specific steps.&#x20;

**Step 1:** Open the Toril Fantasy official website (URL: http://beta.toril.io/ ), login and click on "Market".

![](../.gitbook/assets/如何1.png)

**Step 2:** Enter the Torrell NFT marketplace and filter through the interface to find the Torrell NFT you like.

![](../.gitbook/assets/如何2.png)

**Step 3:** Find more details about the Torrell NFTs available on the market and click on "Authorize" to confirm before purchasing your preferred Torrell NFT for the first time.

![](../.gitbook/assets/如何3.png)

![](../.gitbook/assets/如何4.png)

**Step 4:** After successful authorization, refresh the page, then click the "Buy" button, and then click "Confirm" in the pop-up confirmation page.

![](../.gitbook/assets/如何5.png)

**Step 5:** After successfully purchasing Toril NFT, click the "Assets" button on the navigation bar, then click the "Wallet Assets" column to view your digital asset details, and find your purchased Toril NFT in the "Heroes" section.

![](../.gitbook/assets/如何6.png)
