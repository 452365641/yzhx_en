# What is Mining Power

Toril NFT in Toril Fantasy can be pledged to Toril Mining Pool for mining. The better the quality of the Toril NFT, the higher the base arithmetic power and the more revenue you get. In addition, the mining gain obtained from the arithmetic power is not set in stone, it will be affected by the arithmetic power and mining pool rewards.
